package com.example.trader.interactivework;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Activity2 extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        Button b = (Button) findViewById(R.id.button);
        Button b2 = (Button) findViewById(R.id.button2);
        Button b3 = (Button) findViewById(R.id.button3);
        Button b4 = (Button) findViewById(R.id.button4);
        b.setOnClickListener(this);
        b2.setOnClickListener(this);
        b3.setOnClickListener(this);
        b4.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button:
                Intent intent1 = new Intent(this,Activity4.class);
                startActivity(intent1);
                break;
            case R.id.button2:
                Intent intent = new Intent(this,Activity3.class);
                startActivity(intent);
                break;
            case R.id.button3:
                Intent intent2 = new Intent(this,Activity5.class);
                startActivity(intent2);
                break;
            case R.id.button4:
                Intent intent3 = new Intent(this,Activity6.class);
                startActivity(intent3);
                break;
            case R.id.button5:
                Intent intent4 = new Intent(this,GroupChatActivity.class);
                startActivity(intent4);
                break;
        }
    }

    public void GroupchatActiv(View view) {


    }
}

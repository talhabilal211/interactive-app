package com.example.trader.interactivework;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class Login extends AppCompatActivity {
    EditText e_mail, passw;
    Button Signin, reset;
    Button signup;
    ProgressBar po;
    int backButtonCount2 = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        e_mail = (EditText) findViewById(R.id.user);
        passw = (EditText) findViewById(R.id.pass);
        Signin = (Button) findViewById(R.id.signin);
        reset = (Button) findViewById(R.id.Reset);
        signup = (Button) findViewById(R.id.Signup);
        po = (ProgressBar) findViewById(R.id.pro);

        Signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userlogin();
            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Login.this, SignUp.class);
                startActivity(i);
            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
                LayoutInflater inflater = getLayoutInflater();
                final View dailogview = inflater.inflate(R.layout.custom_layout, null);
                builder.setView(dailogview);
                final EditText editTextname = (EditText) dailogview.findViewById(R.id.editemail);
                final Button buttonname = (Button) dailogview.findViewById(R.id.btnadd);
                // builder.setTitle(Html.fromHtml("<font color='#FF7F27'>Reset your Password:</font>"));
                //builder.setTitle("Reset your Password:");
                final AlertDialog alertDialog = builder.create();
                alertDialog.show();
                buttonname.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String usermail = editTextname.getText().toString().trim();

                        if (TextUtils.isEmpty(usermail)) {
                            editTextname.setError("Enter Your Register Email");
                            return;
                        }


                        if (usermail.equals("")) {
                            Toast.makeText(Login.this, "Please enter your registered email ID", Toast.LENGTH_SHORT).show();
                        } else {
                            FirebaseAuth.getInstance()
                                    .sendPasswordResetEmail(usermail).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Toast.makeText(Login.this, "Password reset email sent!", Toast.LENGTH_SHORT).show();
                                        finish();
                                        startActivity(new Intent(Login.this, Login.class));
                                    } else {
                                        Toast.makeText(Login.this, "Error in sending password reset email", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }
                        alertDialog.dismiss();


                    }
                });

            }
        });
    }

    public void userlogin() {
        String usereamil = e_mail.getText().toString().trim();
        String password = passw.getText().toString().trim();
        if (usereamil.isEmpty()) {
            e_mail.setError("Please Enter your Email");
            e_mail.requestFocus();
            return;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(usereamil).matches()) {
            e_mail.setError("Enter valid email");
            e_mail.requestFocus();
            return;


        }
        if (password.isEmpty()) {
            passw.setError("Please Enter your password");
            passw.requestFocus();
            return;
        }
        if (password.length() < 6) {
            passw.setError("Password length should be greater than 6");
            passw.requestFocus();
            return;
        }
        po.setVisibility(View.VISIBLE);
        FirebaseAuth.getInstance().signInWithEmailAndPassword(usereamil, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                po.setVisibility(View.GONE);
                if (task.isSuccessful()) {
                    finish();

                    Intent i = new Intent(Login.this, Activity3.class);
                    startActivity(i);

                } else {

                    Toast.makeText(Login.this, task.getException().getMessage(),
                            Toast.LENGTH_SHORT).show();

                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            finish();
            startActivity(new Intent(this, Activity3.class));
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        backButtonCount2 = 0;
    }

    @Override
    public void onBackPressed() {
        if (backButtonCount2 >= 1) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            backButtonCount2 = 0;
        } else {
            Toast.makeText(this, "Press the back button again to close the application.", Toast.LENGTH_SHORT).show();
            backButtonCount2++;
        }
    }
}


package com.example.trader.interactivework;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GroupChatActivity extends AppCompatActivity implements View.OnClickListener{

    FirebaseAuth auth;
    FirebaseDatabase database;
    DatabaseReference messagedb;
    MessageAdapter messageAdapter;
    List<Message> messages;
    user u;
    RecyclerView recyclerView;
    EditText edt1;
    ImageButton imgbtn;
   // Firebase reference1, reference2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_chat);

        init();


    }

    public void init()
    {
       auth=FirebaseAuth.getInstance();
       database=FirebaseDatabase.getInstance();
       u=new user();

       recyclerView=findViewById(R.id.recyler1);
       edt1=findViewById(R.id.edtmessage1);

       imgbtn=findViewById(R.id.btnsend);
       imgbtn.setOnClickListener(this);
       messages=new ArrayList<>();

    }

    @Override
    protected void onStart() {
        super.onStart();

        final FirebaseUser currentUser=auth.getCurrentUser();
        u.setUid(currentUser.getUid());
        u.setGetEmail(currentUser.getEmail());
        database.getReference("Users").child(currentUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                u=dataSnapshot.getValue(user.class);
                u.setUid(currentUser.getUid());
                AllMethods.name=u.getName();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        messagedb=database.getReference("abc");
        messagedb.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Message message=dataSnapshot.getValue(Message.class);
                message.setKey(dataSnapshot.getKey());
                messages.add(message);
                /*showMessages(messages);*/
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                Message message=dataSnapshot.getValue(Message.class);
                message.setKey(dataSnapshot.getKey());
                List<Message> newmessage=new ArrayList<Message>();

                for(Message m:messages)
                {
                    if(m.getKey().equals(message.getKey()))
                    {

                        newmessage.add(message);
                    }
                    else
                    {
                        newmessage.add(m);
                    }
                }
                messages=newmessage;
                /*showMessages(messages);*/
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Message message=dataSnapshot.getValue(Message.class);
                message.setKey(dataSnapshot.getKey());
                List<Message> newmessage=new ArrayList<Message>();

                for(Message m:messages)
                {
                    if(!m.getKey().equals(message.getKey()))
                    {

                        newmessage.add(m);
                    }
                }
                messages=newmessage;
                /*showMessages(messages);*/
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        messages=new ArrayList<>();
    }

   /* private void showMessages(List<Message> messages) {

        recyclerView.setLayoutManager(new LinearLayoutManager(GroupChatActivity.this));
        messageAdapter =new MessageAdapter(this,messages,messagedb);
        recyclerView.setAdapter(messageAdapter);
    }*/

    @Override
    public void onClick(View v) {

        Message message=new Message(edt1.getText().toString(),u.getName());
        edt1.setText("");

        messagedb.push().setValue(message);
        if(!edt1.getText().toString().equals("")){
            Map<String, String> map = new HashMap<String, String>();
            map.put("message", edt1.getText().toString());
         //   map.put("user", UserDetails.username);
            messagedb.push().setValue(map);
            //reference2.push().setValue(map);
            edt1.setText("");
        }


    }
}

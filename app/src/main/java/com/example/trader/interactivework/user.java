package com.example.trader.interactivework;

public class user {

    String uid;
    String name;
    String getEmail;

    public String getUid() {
        return uid;
    }

    public String getName() {
        return name;
    }

    public String getGetEmail() {
        return getEmail;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGetEmail(String getEmail) {
        this.getEmail = getEmail;
    }

    @Override
    public String toString() {
        return "user{" +
                "uid='" + uid + '\'' +
                ", name='" + name + '\'' +
                ", getEmail='" + getEmail + '\'' +
                '}';
    }
}

package com.example.trader.interactivework;

public class Message {

    String message;
    String name;
    String Key;

    public Message() {

    }

    public Message(String message, String name) {
        this.message = message;
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public String getName() {
        return name;
    }

    public String getKey() {
        return Key;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setKey(String key) {
        Key = key;
    }

    @Override
    public String toString() {
        return "Message{" +
                "message='" + message + '\'' +
                ", name='" + name + '\'' +
                ", Key='" + Key + '\'' +
                '}';
    }
}

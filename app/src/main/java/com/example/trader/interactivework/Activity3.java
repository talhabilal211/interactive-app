package com.example.trader.interactivework;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

public class Activity3 extends AppCompatActivity implements OnClickListener {
    private static final String FILE_NAME = "example.txt";
    private static final int choose = 101;
    DatabaseReference databaseReference, groupmsgkey;
    MultiAutoCompleteTextView mainview;
    ArrayList<String> childadd = new ArrayList<>();
    String copydata, chatmsg;
    Uri uriimg;
    String imgdown;
    ImageView im;
    LinearLayout rootView;
    String id;
    ProgressBar po;
    Button btn_bold, btn_italic, btn_normal, btn_size, btn_copydata, btn_new, btn_save, logout, insert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_3);
        btn_bold = (Button) findViewById(R.id.boldtext);
        btn_italic = (Button) findViewById(R.id.italictext);
        btn_normal = (Button) findViewById(R.id.normaltext);
        im = (ImageView) findViewById(R.id.img);
        rootView = findViewById(R.id.rootLayout);
        po = (ProgressBar) findViewById(R.id.pro);
        btn_size = (Button) findViewById(R.id.fontsize);
        btn_copydata = (Button) findViewById(R.id.copytext);
        btn_new = (Button) findViewById(R.id.newdoc);
        btn_save = (Button) findViewById(R.id.savedoc);
        logout = (Button) findViewById(R.id.logout);
        insert = (Button) findViewById(R.id.imagepic);
        id = FirebaseAuth.getInstance().getCurrentUser().getUid();
        databaseReference = FirebaseDatabase.getInstance().getReference("Users").child("abc");
        mainview = (MultiAutoCompleteTextView) findViewById(R.id.mainpart);
        btn_bold.setOnClickListener(this);
        btn_italic.setOnClickListener(this);
        btn_normal.setOnClickListener(this);
        btn_size.setOnClickListener(this);
        btn_new.setOnClickListener(this);
        btn_copydata.setOnClickListener(this);
        registerForContextMenu(btn_size);
        clickListeners();
    }

    private void clickListeners() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                Toast.makeText(this, "please give permission", Toast.LENGTH_SHORT).show();
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        1009);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
        }
        findViewById(R.id.btnChat).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Activity3.this, ChatActivity.class);
                final int random = new Random().nextInt(61) + 20;
                intent.putExtra("CHAT_ID", random);
                startActivity(intent);
                finish();
            }
        });
    }

    private void getPermission() {

    }

    private void loaduserinfo() {

        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            if (user.getPhotoUrl() != null) {
                String ss = user.getPhotoUrl().toString();
                Glide.with(this).load(user.getPhotoUrl().toString()).into(im);

            }

        }


    }

    public void onClick(View view) {
        if (view == btn_bold) {
            Typeface setfontstyle = Typeface.defaultFromStyle(Typeface.BOLD);
            mainview.setTypeface(setfontstyle);
            Toast.makeText(getApplicationContext(), "Bold", Toast.LENGTH_LONG).show();
        } else if (view == btn_italic) {
            Typeface setfontstyle = Typeface.defaultFromStyle(Typeface.ITALIC);
            mainview.setTypeface(setfontstyle);
            Toast.makeText(getApplicationContext(), "Italic", Toast.LENGTH_LONG).show();
        } else if (view == btn_normal) {
            Typeface setfontstyle = Typeface.defaultFromStyle(Typeface.NORMAL);
            mainview.setTypeface(setfontstyle);
            Toast.makeText(getApplicationContext(), "Normal", Toast.LENGTH_LONG).show();
        } else if (view == btn_size) {
            view.showContextMenu();
            Toast.makeText(getApplicationContext(), "Font Size", Toast.LENGTH_LONG).show();
        } else if (view == btn_new) {
            String appnametitle = "Notepoint";
            setTitle(appnametitle + " - New Document.txt");
            mainview.setText("");
            mainview.setHint("New Document");
            mainview.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            Toast.makeText(getApplicationContext(), "New Document", Toast.LENGTH_LONG).show();
        } else if (view == btn_copydata) {
            // copydata = mainview.getText().toString();
            copydata = mainview.getText().toString();
            Toast.makeText(getApplicationContext(), "Copied Successfully !!", Toast.LENGTH_LONG).show();

        }


    }

    private void pushuserdata() {
        String msg = mainview.getText().toString();
        String key = databaseReference.push().getKey();
        if (msg.isEmpty()) {
            mainview.setError("Please enter your Message");
            mainview.requestFocus();
            return;
        } else {


            HashMap<String, Object> msgkey = new HashMap<>();
            databaseReference.updateChildren(msgkey);
            groupmsgkey = databaseReference.child(id);
            HashMap<String, Object> msgkeyinfo = new HashMap<>();
            msgkeyinfo.put("message", msg);


            groupmsgkey.updateChildren(msgkeyinfo);

            save();
        }
    }

    private void save() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        if (user != null && imgdown != null) {
            UserProfileChangeRequest profileimg;
            profileimg = new UserProfileChangeRequest.Builder().setDisplayName(chatmsg).setPhotoUri(Uri.parse(imgdown)).build();

            user.updateProfile(profileimg).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        Toast.makeText(getApplicationContext(), "Profil pic is upload", Toast.LENGTH_SHORT).show();
                        saveImageToTheGallery();
                    } else {
                        Toast.makeText(getApplicationContext(), "not", Toast.LENGTH_SHORT).show();
                    }

                }
            });
        }
    }

    private void getmsg(DataSnapshot dataSnapshot) {

        Iterator iterator = dataSnapshot.getChildren().iterator();


        while (iterator.hasNext()) {
            String a = dataSnapshot.getKey();
            if (id.equals(a)) {
                String chatmsg = (String) ((DataSnapshot) iterator.next()).getValue();

                mainview.setText(chatmsg + "\n");
            }


        }
    }

    public void chatnot() {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                childadd.clear();
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    chatmsg = data.getValue().toString();
                    String a = data.getKey().toString();
                    if (id.equals(a)) {
                        if (chatmsg.contains("{message=")) {
                            chatmsg = chatmsg.replace("{message=", "");
                        }
                        if (chatmsg.contains("}")) {
                            chatmsg = chatmsg.replace("}", "");
                        }
                        mainview.setText(chatmsg + "\n");

                    }


                }
//                for(int i=0;i<childadd.size();i++)
//                {
//                    if(id.equals(childadd.get(i)))
//                    {
//
//                    }
//                }


//
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }


    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        menu.setHeaderTitle("Select Font Size");
        menu.add(0, v.getId(), 0, "5+");
        menu.add(0, v.getId(), 0, "10+");
        menu.add(0, v.getId(), 0, "20+");
        menu.add(0, v.getId(), 0, "25+");
        menu.add(0, v.getId(), 0, "30+");
        menu.add(0, v.getId(), 0, "35+");
        menu.add(0, v.getId(), 0, "40+");
        menu.add(0, v.getId(), 0, "45+");
        menu.add(0, v.getId(), 0, "50");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        if (item.getTitle() == "5+") {
            mainview.setTextSize(TypedValue.COMPLEX_UNIT_SP, 8);
        } else if (item.getTitle() == "10+") {
            mainview.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        } else if (item.getTitle() == "20+") {
            mainview.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        } else if (item.getTitle() == "25+") {
            mainview.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);
        } else if (item.getTitle() == "30+") {
            mainview.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);
        } else if (item.getTitle() == "35+") {
            mainview.setTextSize(TypedValue.COMPLEX_UNIT_SP, 33);
        } else if (item.getTitle() == "40+") {
            mainview.setTextSize(TypedValue.COMPLEX_UNIT_SP, 43);
        } else if (item.getTitle() == "45+") {
            mainview.setTextSize(TypedValue.COMPLEX_UNIT_SP, 48);
        } else if (item.getTitle() == "50") {
            mainview.setTextSize(TypedValue.COMPLEX_UNIT_SP, 50);
        } else {
            Toast.makeText(getApplicationContext(), "Something is Worng", Toast.LENGTH_LONG).show();
        }
        return true;
    }

    public void save(View v) {
        pushuserdata();

    }

    public void logout(View v) {


        FirebaseAuth.getInstance().signOut();
        finish();
        startActivity(new Intent(this, Login.class));
    }

    public void inse(View v) {
        showimg();

    }

    private void showimg() {
        Intent i = new Intent();
        i.setType("image/*");
        i.setAction(i.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(i, "Choose your dp"), choose);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == choose && resultCode == RESULT_OK && data != null && data.getData() != null) {
            uriimg = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uriimg);
                im.setImageBitmap(bitmap);
                uploadimaetofirbase();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private void uploadimaetofirbase() {

        StorageReference profileimgref = FirebaseStorage.getInstance().getReference("profilepics/" + System.currentTimeMillis() + ".jpg");
        if (uriimg != null)
            po.setVisibility(View.VISIBLE);

        profileimgref.putFile(uriimg).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                po.setVisibility(View.GONE);


                imgdown = taskSnapshot.getDownloadUrl().toString();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
//                po.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void load(View v) {
        chatnot();
        loaduserinfo();
//        databaseReference.addChildEventListener(new ChildEventListener() {
//            @Override
//            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//
//                if(dataSnapshot.exists())
//                {
//
//
//                }
//
//            }
//
//            @Override
//            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//                if(dataSnapshot.exists())
//                {
//
//                    //getmsg(dataSnapshot);
//                }
//            }
//
//            @Override
//            public void onChildRemoved(DataSnapshot dataSnapshot) {
//
//            }
//
//            @Override
//            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
    }

    private void saveImageToTheGallery() {
        // View v = rootView.getRootView();
        rootView.setDrawingCacheEnabled(true);
        Bitmap b = rootView.getDrawingCache();
        String extr = Environment.getExternalStorageDirectory().toString();
        File myPath = new File(extr, "Images" + ".jpg");
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(myPath);
            b.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
            MediaStore.Images.Media.insertImage(getContentResolver(), b,
                    "Screen", "screen");
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 1009: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }
}

package com.example.trader.interactivework;

import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by Talha Bilal on 05 July, 2019
 * Email: talha.bilal@alnafay.com
 * Company: AL-NAFAY IT SOLUTIONS (https://alnafay.com/)
 */
public class ChatMessage {

    private String username;
    private String message;
    private String senderID;

    public ChatMessage(String username, String message) {
        this.username = username;
        this.message = message;
        this.senderID = FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    public String getSenderID() {
        return senderID;
    }

    public void setSenderID(String senderID) {
        this.senderID = senderID;
    }

    public ChatMessage() {
    } // you need an empty constructor to get your object from the DataSnapshot (cf. MainActivity)

    public String getUsername() {
        return username;
    }

    public String getMessage() {
        return message;
    }
}

package com.example.trader.interactivework;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;

public class SignUp extends AppCompatActivity {

    EditText e_mail, passw;
    Button Signup;
    ProgressBar po;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        e_mail = (EditText) findViewById(R.id.user);
        passw = (EditText) findViewById(R.id.pass);
        Signup = (Button) findViewById(R.id.signup);
        po = (ProgressBar) findViewById(R.id.pro);
        Signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userreg();
            }
        });
    }


    public void userreg() {

        String usereamil = e_mail.getText().toString().trim();
        String password = passw.getText().toString().trim();
        if (usereamil.isEmpty()) {
            e_mail.setError("Please Enter your Email");
            e_mail.requestFocus();
            return;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(usereamil).matches()) {
            e_mail.setError("Enter valid email");
            e_mail.requestFocus();
            return;


        }
        if (password.isEmpty()) {
            passw.setError("Please Enter your password");
            passw.requestFocus();
            return;
        }
        if (password.length() < 6) {
            passw.setError("Password length should be greater than 6");
            passw.requestFocus();
            return;
        }
        po.setVisibility(View.VISIBLE);
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(usereamil, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            po.setVisibility(View.GONE);
                            Intent i = new Intent(getApplicationContext(), Activity3.class);
                            startActivity(i);
                        } else {
                            // If sign in fails, display a message to the user.
                            if (task.getException() instanceof FirebaseAuthUserCollisionException) {
                                Toast.makeText(SignUp.this, "Email Already exist",
                                        Toast.LENGTH_SHORT).show();
                                po.setVisibility(View.GONE);
                            } else {
                                Toast.makeText(SignUp.this, task.getException().getMessage(),
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });
    }
}
